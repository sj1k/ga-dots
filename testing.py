import physics
import time

class Dot(physics.AcceleratingEntity):
    acceleration_multiplier = 0.03
    
    def collide(self, entity):
        print(str(self)+" hit "+str(entity))

class Goal(physics.StaticEntity):
    position = (5, 5)


physics.add_collisions_between(Dot, Goal)

goal1 = Goal()

dt1 = Dot()
dt2 = Dot()
dt3 = Dot()
dt1.set_acceleration(1,1)
dt2.set_acceleration(0,1)
dt3.set_acceleration(1,0)
for x in range(1000):
    time.sleep(0.3)
    physics.update()
    print(str(dt1.position)+"\t"+str(dt2.position)+"\t"+str(dt3.position))

exit()
