import pygame
import sys


def init():
    pygame.init()
    window = pygame.display.set_mode((500, 500))
    return window


def draw_population(window, population):
    window.fill((255, 255, 255))
    for dot in population:
        position = tuple(map(int, dot.position))
        if dot.champion:
            pygame.draw.circle(window, (0, 255, 0), position, 5)
        else:
            pygame.draw.circle(window, (10, 10, 10), position, 2)
    return None


def draw_obstacles(window, obstacles):
    for obs in obstacles:
        x, y = tuple(map(int, obs.position))
        w, h = obs.size
        pygame.draw.rect(window, (40, 40, 40), (x - w, y - h, w * 2, h * 2))


def draw_goal(window, goal):
    pygame.draw.circle(window, (255, 0, 0), goal.position, goal.size[0])
    return None


def handle_events():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit(0)
    return None


def update():
    pygame.display.update()
    return None
