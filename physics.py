"""
The physics module
Makes physical calculations for different types of entities
"""

import enum
import math
import time
from types import *

import events

entities = {}    #stores all entities
moving_list = {} #stores all entity types that move
collisions = []  #stores all the collisions that need to be calculated

def add_entity(entity):
    """
    Adds an entity to the physics calculation
    Is called by BasicEntity.__init__
    All sub-classes should call super to triger this or call this function manually
    """
    category = type(entity).__name__
    if (category not in entities):
        entities[category] = []
        moving_list[category] = callable(getattr(entity, "physical_move", None))
    entities[category].append(entity)

def remove_entity(entity):
    """
    Removes an entity from the physics calculation if present
    Is called by BasicEntity.__del__
    All sub-classes should call super().__del__ to triger this or call this function manually
    """
    category = type(entity).__name__
    type_list = entities.get(category, [])
    if entity in type_list:
        type_list.remove(entity)


def add_collisions_between(entity_type1, entity_type2):
    """
    Adds collision detection for the 2 entity types (for example Dots and Goal)
    Henceforth whenever an update occurs the physics engine will 
        automatically check if any entities from that 2 types collide
    When a collision is detected it will call the .collide method 
        of the instance of the first entity type that was passed to this function
        and will pass the instance of the second entity_type that was passed as parameter
    """
    collisions.append((entity_type1.__name__, entity_type2.__name__))


def is_dynamic(entity_type_name):
    """
    Used to check if an entity type name needs physical updates
    Essentially just checks if it contains a method named "physical_move"
    """
    return moving_list[entity_type_name]

class VolumeModel(enum.Enum):
    """
    An enumeration of collision models for the physics
    """
    BOX = 0
    SPHERE = 1
    CONTAINING_BOX = 2
    CONTAINING_SPHERE = 3

    
    @staticmethod
    def get_collision_function(e1, e2):
        def box_box_collision(entity1, entity2):
            px1, py1 = entity1.position
            px2, py2 = entity2.position
            dx = px1 - px2
            dy = py1 - py2

            cx1, cy1 = entity1.size
            cx2, cy2 = entity2.size
            cx = cx1 + cx2
            cy = cy1 + cy2

            return (math.fabs(dx)<cx and math.fabs(dy)<cy)

        def sphere_sphere_collision(entity1, entity2):
            px1, py1 = entity1.position
            px2, py2 = entity2.position
            dx = px1 - px2
            dy = py1 - py2
            
            cx1, cy1 = entity1.size
            cx2, cy2 = entity2.size
            cx = cx1 + cx2
            cy = cy1 + cy2

            return (((dx/cx)**2) + ((dy/cy)**2)) < 1

        if (e1.volume_model == VolumeModel.SPHERE and e2.volume_model == VolumeModel.SPHERE):
            return sphere_sphere_collision
        return box_box_collision

def update(time_passed=1):
    for category, entity_list in entities.items():
        if (is_dynamic(category)):
            for entity in entity_list:
                entity.physical_move(time_passed)
    
    for category1, category2 in collisions:
        category1 = entities.get(category1,None)
        category2 = entities.get(category2,None)
        if not (category1 and category2):
            pass
        collision_func = VolumeModel.get_collision_function(category1[0], category2[0])

        for entity1 in category1:
            for entity2 in category2:
                if collision_func(entity1, entity2):
                    entity1.collide(entity2)


    
class BasicEntity:
    """
    A basic entity that respresents an abstract class all other entities should inherit from
    """
    position = (0,0)               #center
    size = (1,1)                   #radius not diameter
    rotation = 0                   #in radians
    volume_model = VolumeModel.BOX #set up to allow physics to work
    living = True                  # Lets the universe check if its living.

    def __init__(self):
        self.living = True

    def __del__(self):
        self.living = False
        
class StaticEntity(BasicEntity):
    """
    An entity that is static as in it does not move after it has been assigned its final position
    Any subclass should share this attribute
    """


class MovingEntity(BasicEntity):
    """
    An entity that moves with a fixed speed in a straight line
    Or spins around it's own axis
    """

    velocity = (0,0)
    velocity_multiplier = 1.0

    angular_velocity = 0
    angular_velocity_multiplier = 1.0

    def set_velocity(self, x, y):
        """
        Used to assign a new velocity to the Entity
        """
        total_velocity = math.sqrt((x**2)+(y**2))
        if (total_velocity > 1.0):
            x = x / total_velocity
            y = y / total_velocity
        self.velocity = (x, y)

    def set_angular_velocity(self, angular_velocity):
        """
        Used to assign a new spin to the Entity
        """
        total_angular_velocity = math.fabs(angular_velocity)
        if (total_angular_velocity > 1.0):
            angular_velocity = angular_velocity/total_angular_velocity
        self.angular_velocity = angular_velocity

    def physical_move(self, t):
        """
        Moves the entity according to the current speed
        Rotates the entity according to the current spin
        The parameter t defines the time that has passed since the last call
        """
        vx, vy = self.velocity
        sx, sy = self.position
        vm = self.velocity_multiplier * t
        sx = sx + vx * vm
        sy = sy + vy * vm
        self.position = (sx, sy)
        
        self.rotation = self.rotation + self.angular_velocity * self.angular_velocity_multiplier * t

class AcceleratingEntity(MovingEntity):
    """
    For entities which use accelerated movement
    Or accelerate around their own axis
    """
    acceleration = (0,0)
    acceleration_multiplier = 1.0
    
    angular_acceleration = 0
    angular_acceleration_multiplier = 1.0
    
    def set_acceleration(self, x, y):
        """
        Used to assign a new acceleration
        """
        total_acceleration = math.sqrt((x**2)+(y**2))
        if (total_acceleration > 1.0):
            x = x / total_acceleration
            y = y / total_acceleration
        self.acceleration = (x, y)

    def set_angular_acceleration(self, angular_acceleration):
        """
        Used to assign a new angular acceleration
        """
        total_angular_acceleration = math.fabs(angular_acceleration)
        if (total_angular_acceleration > 1.0):
            angular_acceleration = angular_acceleration / total_angular_acceleration
        self.angular_acceleration = (x, y)
        
    def physical_move(self, t):
        """
        Moves the entity according to the current acceleratio and speed
        Rotates the entity according to the current spin
        The parameter t defines the time that has passed since the last call
        """
        ax, ay = self.acceleration
        vx, vy = self.velocity
        am = self.acceleration_multiplier * t
        vx = vx + ax * am
        vy = vy + ay * am
        self.set_velocity(vx, vy)
        
        self.set_angular_velocity(self.angular_velocity + self.angular_acceleration * self.angular_acceleration_multiplier * t)

        super().physical_move(t)


class Universe:

    entities = {}
    moving = {}
    collisions = []

    def __init__(self, entities, tickrate=1):
        self.tickrate = tickrate
        for entity in entities:
            self.add_entity(entity)

    def __iter__(self):
        while True:
            time.sleep(self.tickrate)
            yield events.TICK
        raise StopIteration

    def add_entities(self, entities):
        for ent in entities:
            self.add_entity(ent)
        return None

    def add_entity(self, entity):
        category = type(entity).__name__
        if (category not in self.entities):
            self.entities[category] = []
            fnc = callable(getattr(entity, "physical_move", None))
            self.moving[category] = fnc
        self.entities[category].append(entity)
        return None

    def add_collision(self, entity1, entity2):
        self.collisions.append((entity1.__name__, entity2.__name__))
        return None

    def remove_entities(self, entities):
        for ent in entities:
            self.remove_entity(ent)
        return None

    def remove_entity(self, entity):
        category = type(entity).__name__
        type_list = self.entities.get(category, [])
        if entity in type_list:
            self.entities[category].remove(entity)
        return None

    def is_dynamic(self, entity_type):
        return self.moving[entity_type]

    def update(self, time_passed=1):
        self.move(time_passed)
        self.collide()
        return None

    def move(self, time_passed=1):
        for category, entity_list in self.entities.items():
            if self.is_dynamic(category):
                for entity in entity_list:
                    entity.physical_move(time_passed)
        return None

    def collide(self):
        for category1, category2 in self.collisions:
            category1 = self.entities.get(category1, None)
            category2 = self.entities.get(category2, None)
            if not (category1 and category2):
                continue

            fnc = VolumeModel.get_collision_function(category1[0], category2[0])
            for entity1 in category1:
                for entity2 in category2:
                    if fnc(entity1, entity2):
                        entity1.collide(entity2)
        return None
